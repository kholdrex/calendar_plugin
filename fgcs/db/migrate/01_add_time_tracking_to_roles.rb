class AddTimeTrackingToRoles < ActiveRecord::Migration
  def up
   add_column :roles, :time_tracking, :boolean, :default => false
  end

  def down
    remove_column :roles, :time_tracking
  end
end