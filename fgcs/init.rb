# encoding: utf-8
#Encoding.default_external = Encoding::UTF_8
#Encoding.default_internal = Encoding::UTF_8

Redmine::Plugin.register :fgcs do
  require_dependency 'hook_listener'
  name 'Fgcs plugin'
  author 'Alexandr Kholodniak'
  description 'The plugin allows you to expand the visibility settings of tasks, as well as to change the calendar.'
  version '0.0.1'
  url 'http://works.kholodniak.ru/redmine'
  author_url 'http://kholodniak.ru/'

  module CalendarPatch
    def self.included(base) # :nodoc:
      base.class_eval do

        def first_wday
          case Setting.start_of_week.to_i
            when 1
              @first_dow ||= (1 - 1)%7 + 1
            when 6
              @first_dow ||= (6 - 1)%7 + 1
            when 7
              @first_dow ||= (1 - 1)%7 + 1
            else
              @first_dow ||= (1 - 1)%7 + 1
          end
        end
        def events_on_with_plugin(day)
          @events = @events.uniq{ |x| x.id }
          result = @events.select{|x| x.is_a?(Issue) && x.due_date.present? && x.start_date.present? && x.start_date < day && x.due_date > day}
          result += @events.select{|x| x.is_a?(Issue) && x.due_date.present? && x.due_date == day || x.is_a?(Issue) && x.start_date.present? && x.start_date == day}
          if day == Date.today
            result += @events.select{|x| x.is_a?(Issue) && x.start_date.present? && x.start_date == day }
            result = result.uniq{|x| x.id}
          end
          if day <= Date.today
            dead = @events.select{|x| x.is_a?(Issue) && x.due_date.present? && x.due_date < day}.select{|x| x.closed_on == nil}.uniq{|x| x.id}
            if day == Date.today
              dead += @events.select{|x| x.is_a?(Issue) && x.due_date.present? && x.due_date == day}.select{|x| x.end_time.present? && DateTime.now.hour.to_i >= x.end_time.split(':')[0].to_i && DateTime.now.min.to_i >= x.end_time.split(':')[1].to_i}
            end
            dead = dead.uniq{|x| x.id}.select{ |x| x.status.name != "Согласовать" && x.status.name != "Решена" && x.status.name != "Приостановлена"} if dead.present?
          end
          result -= dead if dead.present?
          warning = result.select{ |x| x.priority.position_name.index("high")}
          result -= warning
          new = result.select{|x| x.status.name == "Новая" }
          result -= new
          result = new + warning + result
          result = dead + result if dead.present?
          result
        end
        alias_method_chain :events_on, :plugin
      end
    end
  end
  module CalendarsControllerPatch
    def self.included(base) # :nodoc:
      base.class_eval do
        def show_with_plugin
          if params[:year] and params[:year].to_i > 1900
            @year = params[:year].to_i
            if params[:month] and params[:month].to_i > 0 and params[:month].to_i < 13
              @month = params[:month].to_i
            end
          end
          @year ||= Date.today.year
          @month ||= Date.today.month

          @calendar = Redmine::Helpers::Calendar.new(Date.civil(@year, @month, 1), current_language, :month)
          retrieve_query
          @query.group_by = nil
          if @query.valid?
            events = []
            events += @query.issues(:include => [:tracker, :assigned_to, :priority]
            )
            events += @query.versions()

            @calendar.events = events
          end

          render :action => 'show', :layout => false if request.xhr?
        end
        alias_method_chain :show, :plugin
      end
    end
  end
  module IssuePatch
    def self.included(base) # :nodoc:
      base.class_eval do
        def end_time
          if IssueCustomField.where(:name => "Время окончания").present?
            idn = IssueCustomField.where(:name => "Время окончания").first.id
            result = CustomValue.where(:customized_type => "Issue", :customized_id => self.id, :custom_field_id => idn).first
            result = result.value if result.present?
            result = nil if result.nil?
            result
          else
            nil
          end
        end
        def self.visible_condition(user, options={})
          Project.allowed_to_condition(user, :view_issues, options) do |role, user|
            if [ 'default', 'own' ].include?(role.issues_visibility)
              user_ids = [user.id] + user.groups.map(&:id).compact
              watched_issues = Issue.watched_by(user).map(&:id)
              watched_issues_clause = watched_issues.empty? ? "" : " OR #{table_name}.id IN (#{watched_issues.join(',')})"
            end
            if user.logged?
              case role.issues_visibility
                when 'all'
                  nil
                when 'default'
                  "(#{table_name}.is_private = #{connection.quoted_false} OR #{table_name}.author_id = #{user.id} OR #{table_name}.assigned_to_id IN (#{user_ids.join(',')}) #{watched_issues_clause})"
                when 'own'
                  "(#{table_name}.author_id = #{user.id} OR #{table_name}.assigned_to_id IN (#{user_ids.join(',')}) #{watched_issues_clause})"
                else
                  '1=0'
              end
            else
              "(#{table_name}.is_private = #{connection.quoted_false})"
            end
          end
        end

        def visible?(usr=nil)
          (usr || User.current).allowed_to?(:view_issues, self.project) do |role, user|
            if user.logged?
              case role.issues_visibility
                when 'all'
                  true
                when 'default'
                  !self.is_private? || (self.author == user || self.watched_by?(user) || user.is_or_belongs_to?(assigned_to))
                when 'own'
                  self.author == user || self.watched_by?(user) || user.is_or_belongs_to?(assigned_to)
                else
                  false
              end
            else
              !self.is_private?
            end
          end
        end
        def addable_watcher_users
          users = self.project.users.sort - self.watcher_users
          users.reject! {|user| !user.allowed_to?(:view_issues, self.project)}
          users
        end
      end
    end
  end
  module ApplicationHelperPatch
    def get_calendar_styles(issue, day)

      is_dead = issue.status.name == "Решена"
      is_warning = issue.status.name == "Согласовать" || issue.status.name == "Приостановлена"
      result = ""
      if issue.start_date.eql?(day)
        result += 'left_success '
      end
      if issue.due_date.eql?(day) && !is_dead
        result += 'right_dead '
      end

      if is_dead && issue.due_date.eql?(day) && !is_warning
        result += 'right_success'
      end

      if is_warning && issue.due_date.eql?(day)
        result += 'right_warning'
      end
      result
    end

    def is_calendar_warning_styles?(issue)
      result = ""
      if issue.priority.position_name.index("high")
        result += 'warning_calendar'
      end
      result
    end

    def get_calendar_overdue(issue, day)
      res = 0
      if issue.end_time.present? && issue.due_date.present? && issue.due_date == Date.today
        if issue.status.name != "Согласовать" && issue.status.name != "Приостановлена" && issue.status.name != "Решена"
          if day == Date.today && DateTime.now.hour.to_i >= issue.end_time.split(':')[0].to_i && DateTime.now.min.to_i >= issue.end_time.split(':')[1].to_i
            res = 1
          end
        end
      else
        res = (day - issue.due_date).day / 86400  if issue.due_date.present?
      end
      res
    end

    def sort_calendar(issues)
      issues
    end

    def calendar_events_on(day, project_id)
      if project_id.nil?
        result = Issue.where("start_date<=? AND due_date>=? AND assigned_to_id=?", day, day, User.current.id)
        if day == Date.today
          result += Issue.where("start_date=? AND assigned_to_id=?", day, User.current.id)
          result = result.uniq{|x| x.id}
        end
        if day <= Date.today
          dead = Issue.where("due_date<? AND assigned_to_id=?", day, User.current.id).select{|x| x.closed_on == nil}.uniq{|x| x.id}
          if day == Date.today
            dead += Issue.where("due_date=? AND assigned_to_id=?", day, User.current.id).select{|x| x.end_time.present? && DateTime.now.hour.to_i >= x.end_time.split(':')[0].to_i && DateTime.now.min.to_i >= x.end_time.split(':')[1].to_i}
          end
          dead = dead.uniq{|x| x.id} if dead.present?
        end
        result -= dead if dead.present?
        warning = result.select{ |x| x.priority.position_name.index("high")}
        result -= warning
        new = result.select{|x| x.status.name == "Новая" }
        result -= new
        result = new + warning +result
        result = dead + result if dead.present?
        result
      else
        result = Issue.where("start_date<=? AND due_date>=? AND project_id=?", day, day, project_id)
        if day == Date.today
          result += Issue.where("start_date=? AND project_id=?", day, project_id)
          result = result.uniq{|x| x.id}
        end
        if day <= Date.today
          dead = Issue.where("due_date<? AND project_id=?", day, project_id).select{|x| x.closed_on == nil}.uniq{|x| x.id}
          if day == Date.today
            dead += Issue.where("due_date=? AND project_id=?", day, project_id).select{|x| x.end_time.present? && DateTime.now.hour.to_i >= x.end_time.split(':')[0].to_i && DateTime.now.min.to_i >= x.end_time.split(':')[1].to_i}
          end
          dead = dead.uniq{|x| x.id} if dead.present?
        end
        result -= dead if dead.present?
        warning = result.select{ |x| x.priority.position_name.index("high")}
        result -= warning
        new = result.select{|x| x.status.name == "Новая" }
        result -= new
        result = new + warning + result
        result = dead + result if dead.present?
        result
      end
      result
    end

  end

  module MyHelperPatch
    def self.included(base) # :nodoc:
      base.class_eval do

        def calendar_items_with_plugin(startdt, enddt)
          Issue.visible.
              where(:project_id => User.current.projects.map(&:id)).
              includes(:project, :tracker, :priority, :assigned_to).
              all
        end

        alias_method_chain :calendar_items, :plugin

      end
    end
  end

  Issue.send(:include, IssuePatch)
  MyHelper.send(:include, MyHelperPatch)
  ApplicationHelper.send(:include, ApplicationHelperPatch)
  Redmine::Helpers::Calendar.send(:include, CalendarPatch)
  CalendarsController.send(:include, CalendarsControllerPatch)
  settings :default => {'empty' => true}, :partial => 'settings/calendar_settings'
end
