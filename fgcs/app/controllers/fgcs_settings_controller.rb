class FgcsSettingsController < ApplicationController
  unloadable
  helper :gantt
  helper :issues
  helper :projects
  helper :queries
  include QueriesHelper
  helper :sort
  include SortHelper
  include Redmine::Export::PDF

  def get_gantt
    @gantt = Redmine::Helpers::Gantt.new(params)
    @gantt.project = Project.where(:id => params[:project_id]).first
    retrieve_query
    @query.group_by = nil
    @gantt.query = @query if @query.valid?

    basename = (@project ? "#{@project.identifier}-" : '') + 'gantt'
    render :partial => 'shared/my_gantt'
  end

  def get_agile

  end

end
